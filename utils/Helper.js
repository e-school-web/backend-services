module.exports = {
	date: (date, delimiter = '/') => {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [day, month, year].join(delimiter);
	},

	lower: (array) => {
		return array.map(item => {
			return Object.keys(item).reduce((c, k) => (c[k.toLowerCase()] = item[k], c), {});
		});
	}
};