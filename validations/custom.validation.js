const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 5) {
    return helpers.message("Şifrə ən azı 5 simvoldan ibarət olmalıdır!");
  }
  //  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
  //     return helpers.message('Şifrədə minimum 1 hərf və 1 rəqəm olmalıdır!');
  //  }
  return value;
};

module.exports = {
  objectId,
  password,
};
