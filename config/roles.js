const allRoles = {
   user: [],
   admin: ['show', 'manage'],
};

const roles = Object.keys(allRoles);
const roleRights = new Map(Object.entries(allRoles));

module.exports = {
   roles,
   roleRights,
};
