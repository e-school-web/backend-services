const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const db = require("../models");

/**
 * get gender types
 * no params
 */

const genderTypes = async () => {
  return db.gender.findAll({ attributes: ["id", "title_az"] });
};

const getGenderTitleByKey = async (key, language) => {
  const gender = await db.gender.findOne({ where: { key } });
  return gender[`title_${language}`];
};

module.exports = {
  genderTypes,
  getGenderTitleByKey,
};
