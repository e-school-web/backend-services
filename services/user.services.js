const httpStatus = require("http-status");
const db = require("../models");
const ApiError = require("../utils/ApiError");
var bcrypt = require("bcryptjs");

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const createUser = async (userBody) => {
  if (await isUsernameTaken(userBody.username)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Username already taken");
  }
  if (userBody.email && (await isEmailTaken(userBody.email))) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  userBody.password = bcrypt.hashSync(userBody.password, 8);
  return await db.user.create(userBody);
};

/**
 *
 * @param {*} phone
 * @returns {Promise<User>}
 */
const isPhoneTaken = async (phone) => {
  return db.user.findOne({
    where: { phone },
  });
};

const isUsernameTaken = async (username) => {
  return db.user.findOne({
    where: { username },
  });
};

const isEmailTaken = async (email) => {
  return db.user.findOne({
    where: { email },
  });
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async (options) => {
  const { page = 1, limit = 10 } = options;
  const results = await db.user.findAll({
    offset: (page - 1) * limit,
    limit,
  });
  const totalResults = await db.user.count();
  return {
    results,
    page,
    limit,
    totalPages: Math.ceil(totalResults / limit),
    totalResults,
  };
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return db.user.findByPk(id);
};

/**
 * Get user by email
 * @param {string} email
 * @returns {Promise<User>}
 */
const getUserByEmail = async (email) => {
  return await db.user.findOne({ where: { email } });
};

/**
 *
 * @param {string} phone
 * @returns {Promise<User>}
 */
const getUserByPhone = async (phone) => {
  return await db.user.findOne({ where: { phone } });
};

/**
 *
 * @param {string} username
 * @returns {Promise<User>}
 */
const getUserByUsername = async (username) => {
  return await db.user.findOne({ where: { username } });
};

const isPasswordMatch = async (user, password) => {
  return await bcrypt.compare(password, user.password);
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Email already taken");
  }
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }
  await user.remove();
  return user;
};

module.exports = {
  createUser,
  isPhoneTaken,
  isUsernameTaken,
  isEmailTaken,
  isPasswordMatch,
  queryUsers,
  getUserById,
  getUserByEmail,
  getUserByPhone,
  getUserByUsername,
  updateUserById,
  deleteUserById,
};
