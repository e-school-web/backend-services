'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const config = require('../../config/config');
const express = require('express');

const router = express.Router();

fs
	.readdirSync(__dirname + '/default')
	.filter(file => {
		return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
	})
	.forEach(file => {
		router.use('/' + file.split('.')[0], require(path.join(__dirname + '/default', file)));
	});

// if (config.env === 'development') {
// 	fs
// 		.readdirSync(__dirname + '/dev')
// 		.filter(file => {
// 			return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
// 		})
// 		.forEach(file => {
// 			// console.log(file.split('.')[0]);
// 			router.use('/' + file.split('.')[0], require(path.join(__dirname + '/dev', file)));
// 		});
// }

module.exports = router;
