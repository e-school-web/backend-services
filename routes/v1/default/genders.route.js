const express = require("express");
const genderController = require("../../../controllers/gender.controller");
// const auth = require("../../../middlewares/auth");

const router = express.Router();

router.route("/").get(genderController.getGenderTypes);

module.exports = router;
