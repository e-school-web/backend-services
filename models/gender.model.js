module.exports = (sequelize, DataTypes) => {
  const Gender = sequelize.define(
    "gender",
    {
      key: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      title_az: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      title_en: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      title_ru: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      underscored: true,
      //timestamps: false for remove updateAt and createAt,
      //tableName: "" define table name,
      //freezeTableName: true how to write table name,
      //paranoid: true for deleted_at at destroy,
    }
  );

  // Gender.bulkCreate([
  //   {
  //     key: "M",
  //     title_az: "Kişi",
  //     title_en: "Male",
  //     title_ru: "Мужчина",
  //   },
  //   {
  //     key: "F",
  //     title_az: "Qadın",
  //     title_en: "Female",
  //     title_ru: "Женщина",
  //   },
  // ]);

  return Gender;
};
