const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync.js");
const { userService, tokenService, genderService } = require("../services");

const getUserByToken = catchAsync(async (req, res) => {
  const refreshToken = req.headers["refreshtoken"];
  const token_data = await tokenService.getTokenDataById(refreshToken);
  let user = await userService.getUserById(token_data.userId);
  user.dataValues.gender = await genderService.getGenderTitleByKey(
    user.gender_key,
    req.headers["language"]
  );
  res.send({ user });
});

module.exports = {
  getUserByToken,
};
