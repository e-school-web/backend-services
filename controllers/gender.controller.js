const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync.js");
const genderService = require("../services/gender.services");

const getGenderTypes = catchAsync(async (req, res) => {
  const genders = await genderService.genderTypes();
  res.status(httpStatus.OK).send(genders);
});

module.exports = {
  getGenderTypes,
};
